import React, { Component } from "react";
import { Nav, Navbar } from "react-bootstrap/";
class Navs extends Component {
  state = {};
  render() {
    return (
      <div>
        {/* Nav.Link  or Link. in Link Component we use "to" attributes instead of "href". */}

        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/">Route-Basic</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link href="/contact">Contact</Nav.Link>
          </Nav>
        </Navbar>
      </div>
    );
  }
}

export default Navs;
