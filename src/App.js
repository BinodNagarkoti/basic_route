import React, { Component } from "react";
import "./App.css";
import Routes from "./route";
import Navs from "./navbar";
import { BrowserRouter as Router } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Navs />
          <Routes />
        </Router>
      </div>
    );
  }
}

export default App;
