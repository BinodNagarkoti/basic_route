import React from "react";
import { Route } from "react-router-dom";
import Home from "./home";
import About from "./about";
import Contact from "./contact";
const Routes = props => {
  return (
    <div>
      <Route path="/about" component={About}>
        {/* <About /> */}
      </Route>
      <Route path="/contact" component={Contact}>
        <Contact />
      </Route>
      <Route exact path="/" component={Contact}>
        <Home />
      </Route>
    </div>
  );
};

export default Routes;
